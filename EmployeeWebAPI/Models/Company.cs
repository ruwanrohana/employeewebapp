﻿using System;
using System.Collections.Generic;

namespace EmployeeWebAPI.Models
{
    public partial class Company
    {
        public Company()
        {
            Employees = new HashSet<Employee>();
        }

        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public byte[] Logo { get; set; }
        public string Website { get; set; }

        public virtual ICollection<Employee> Employees { get; set; }
    }
}
