﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeWebAPI.Models
{
    public class UserWithToken:User
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string Token { get; internal set; }

        public UserWithToken(User user)
        {
            this.UserId = user.UserId;
            this.LoginEmail = user.LoginEmail;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;

            this.SecurityProfile = user.SecurityProfile;
        }

    }
}
