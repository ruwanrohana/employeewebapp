﻿using System;
using System.Collections.Generic;

namespace EmployeeWebAPI.Models
{
    public partial class SecurityProfile
    {
        public SecurityProfile()
        {
            Users = new HashSet<User>();
        }

        public int SecurityProfileId { get; set; }
        public string Name { get; set; }
        public bool? CreatRight { get; set; }
        public bool? ReadRight { get; set; }
        public bool? UpdateRight { get; set; }
        public bool? DeleteRight { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}
