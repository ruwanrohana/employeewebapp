﻿using System;
using System.Collections.Generic;

namespace EmployeeWebAPI.Models
{
    public partial class User
    {
        public int UserId { get; set; }
        public string LoginEmail { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public int SecurityProfileId { get; set; }

        public virtual SecurityProfile SecurityProfile { get; set; }
    }
}
