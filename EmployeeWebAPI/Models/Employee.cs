﻿using System;
using System.Collections.Generic;

namespace EmployeeWebAPI.Models
{
    public partial class Employee
    {
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CompanyId { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Skills { get; set; }

        public virtual Company Company { get; set; }
    }
}
