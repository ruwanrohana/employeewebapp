﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EmployeeWebAPI.Models;

namespace EmployeeWebAPI.Controllers
{
    //[Authorize]
    [Route("api/SecurityProfiles")]
    [ApiController]
    public class SecurityProfilesController : ControllerBase
    {
        private readonly EmployeeContext _context;

        public SecurityProfilesController(EmployeeContext context)
        {
            _context = context;
        }

        // GET: api/SecurityProfiles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SecurityProfile>>> GetSecurityProfiles()
        {
            return await _context.SecurityProfiles.ToListAsync();
        }

        // GET: api/SecurityProfiles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SecurityProfile>> GetSecurityProfile(int id)
        {
            var securityProfile = await _context.SecurityProfiles.FindAsync(id);

            if (securityProfile == null)
            {
                return NotFound();
            }

            return securityProfile;
        }

        // PUT: api/SecurityProfiles/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSecurityProfile(int id, SecurityProfile securityProfile)
        {
            if (id != securityProfile.SecurityProfileId)
            {
                return BadRequest();
            }

            _context.Entry(securityProfile).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SecurityProfileExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SecurityProfiles
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<SecurityProfile>> PostSecurityProfile(SecurityProfile securityProfile)
        {
            _context.SecurityProfiles.Add(securityProfile);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSecurityProfile", new { id = securityProfile.SecurityProfileId }, securityProfile);
        }

        // DELETE: api/SecurityProfiles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<SecurityProfile>> DeleteSecurityProfile(int id)
        {
            var securityProfile = await _context.SecurityProfiles.FindAsync(id);
            if (securityProfile == null)
            {
                return NotFound();
            }

            _context.SecurityProfiles.Remove(securityProfile);
            await _context.SaveChangesAsync();

            return securityProfile;
        }

        private bool SecurityProfileExists(int id)
        {
            return _context.SecurityProfiles.Any(e => e.SecurityProfileId == id);
        }
    }
}
