﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebAppEmployee.Models
{
    public class User
    {
        public int UserId { get; set; }
        public string LoginEmail { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }
        public int SecurityProfileId { get; set; }

        public virtual SecurityProfile SecurityProfile { get; set; }
    }
}
