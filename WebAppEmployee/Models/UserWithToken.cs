﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAppEmployee.Models
{
    public class UserWithToken
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string Token { get; set; }
       
        public UserWithToken()
        {
        }
    }
}
