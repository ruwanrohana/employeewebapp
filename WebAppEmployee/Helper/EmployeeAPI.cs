﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebAppEmployee.Helper
{
    public class EmployeeAPI
    {
        public static string AccessToken { get; set; }
        public HttpClient Initial()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://localhost:44383/");
            return client;
        }

    }
}
