﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebAppEmployee.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAppEmployee.Helper;
using System.Text.Json;
using System.Text;

namespace WebAppEmployee.Controllers
{
    public class CompanyController : Controller
    {

        EmployeeAPI _api = new EmployeeAPI();
        public async Task<IActionResult> Index()
        {
            HttpClient client = _api.Initial();

            List<Company> companies = new List<Company>();           
            HttpResponseMessage res = await client.GetAsync("api/Companies");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                companies = JsonConvert.DeserializeObject<List<Company>>(result);

            }
            return View(companies);
        }

        public ActionResult Create()
        {
            try
            {
                Company companyModel = new Company();
                return View(companyModel);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create(Company companyModel)
        {
            try
            {
                HttpClient client = _api.Initial();
                Company companies = new Company();
                if (ModelState.IsValid)
                {
                    var todocompanyJson = new StringContent(System.Text.Json.JsonSerializer.Serialize(companyModel, null), Encoding.UTF8, "application/json");

                    HttpResponseMessage res =  await client.PostAsync("/api/Companies", todocompanyJson);

                    if (res.IsSuccessStatusCode)
                    {
                        var result = res.Content.ReadAsStringAsync().Result;
                        companies = JsonConvert.DeserializeObject<Company>(result);

                    };
                }
                return View(companies);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        public async Task<IActionResult> Edit(int id)
        {
            try
            {

                HttpClient client = _api.Initial();

                Company companies = new Company();
                HttpResponseMessage res = await client.GetAsync("api/Companies/" + id.ToString());
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    companies = JsonConvert.DeserializeObject<Company>(result);

                }

                return View(companies);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Company companyModel)
        {
            try
            {
                HttpClient client = _api.Initial();
                Company companies = new Company();
                if (ModelState.IsValid)
                {
                  var todocompanyJson = new StringContent(System.Text.Json.JsonSerializer.Serialize(companyModel, null), Encoding.UTF8,
                  "application/json");

                    HttpResponseMessage res = await client.PutAsync("/api/Companies", todocompanyJson);

                    if (res.IsSuccessStatusCode)
                    {
                        var result = res.Content.ReadAsStringAsync().Result;
                        companies = JsonConvert.DeserializeObject<Company>(result);

                    };
                }
                return View(companies);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                HttpClient client = _api.Initial();
                Company companies = new Company();
                HttpResponseMessage res = await client.GetAsync("api/Companies/" + id.ToString());
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    companies = JsonConvert.DeserializeObject<Company>(result);
                }

                return View(companies);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [HttpDelete("Delete")]
        public async Task<IActionResult> Delete(Company companyModel)
        {
            try
            {
                HttpClient client = _api.Initial();
                Company companies = new Company();
                if (ModelState.IsValid)
                {

                    HttpResponseMessage res = await client.DeleteAsync("/api/Companies/"+companyModel.CompanyId.ToString());

                    if (res.IsSuccessStatusCode)
                    {                      
                        return RedirectToAction("Index");
                    };
                }
                return View(companies);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

    }
}
