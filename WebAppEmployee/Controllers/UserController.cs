﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAppEmployee.Helper;
using WebAppEmployee.Models;
using System.Text.Json;
using System.Text;

namespace WebAppEmployee.Controllers
{
    public class UserController : Controller
    {
        EmployeeAPI _api = new EmployeeAPI();
        public async Task<IActionResult> Index()
        {
            HttpClient client = _api.Initial();

            List<User> users=new List<User>();
            HttpResponseMessage res = await client.GetAsync("api/Users");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                users = JsonConvert.DeserializeObject<List<User>>(result);

            }
            return View(users);
        }

        public ActionResult Create()
        {
            try
            {
                User userModel = new User();
                return View(userModel);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create(User userModel)
        {
            try
            {
                HttpClient client = _api.Initial();
                User users = new User();
                if (ModelState.IsValid)
                {
                    var todotodouserJson = new StringContent(System.Text.Json.JsonSerializer.Serialize(userModel, null), Encoding.UTF8, "application/json");

                    HttpResponseMessage res = await client.PostAsync("/api/Users", todotodouserJson);

                    if (res.IsSuccessStatusCode)
                    {
                        var result = res.Content.ReadAsStringAsync().Result;
                        users = JsonConvert.DeserializeObject<User>(result);

                    };
                }
                return View(users);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        public async Task<IActionResult> Edit(int id)
        {
            try
            {

                HttpClient client = _api.Initial();

                User users = new User();
                HttpResponseMessage res = await client.GetAsync("api/Users/" + id.ToString());
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    users = JsonConvert.DeserializeObject<User>(result);

                }

                return View(users);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(User userModel)
        {
            try
            {
                HttpClient client = _api.Initial();
                User users = new User();
                if (ModelState.IsValid)
                {
                    var todoemployeeJson = new StringContent(System.Text.Json.JsonSerializer.Serialize(userModel, null), Encoding.UTF8,
                    "application/json");

                    HttpResponseMessage res = await client.PutAsync("/api/Users", todoemployeeJson);

                    if (res.IsSuccessStatusCode)
                    {
                        var result = res.Content.ReadAsStringAsync().Result;
                        users = JsonConvert.DeserializeObject<User>(result);

                    };
                }
                return View(users);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        public ActionResult Login()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login(User user)
        {
            try
            {
                HttpClient client = _api.Initial();
                UserWithToken users = new UserWithToken();

                var todotodouserJson = new StringContent(System.Text.Json.JsonSerializer.Serialize(user, null), Encoding.UTF8, "application/json");

                HttpResponseMessage res = await client.PostAsync("/api/Users/Login", todotodouserJson);

                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    users = JsonConvert.DeserializeObject<UserWithToken>(result);
                    EmployeeAPI.AccessToken = users.Token;
                    return RedirectToAction("Index", "Employee");
                };

                return View(users);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                HttpClient client = _api.Initial();
                User users = new User();
                HttpResponseMessage res = await client.GetAsync("api/Users/" + id.ToString());
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    users = JsonConvert.DeserializeObject<User>(result);
                }

                return View(users);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(User userModel)
        {
            try
            {
                HttpClient client = _api.Initial();
                User users = new User();
                if (ModelState.IsValid)
                {

                    HttpResponseMessage res = await client.DeleteAsync("/api/Users/" + userModel.UserId.ToString());

                    if (res.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    };
                }
                return View(users);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }
    }
}
