﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAppEmployee.Helper;
using WebAppEmployee.Models;
using System.Text.Json;
using System.Text;

namespace WebAppEmployee.Controllers
{
    public class EmployeeController : Controller
    {
        EmployeeAPI _api = new EmployeeAPI();
        public async Task<IActionResult> Index()
        {
            HttpClient client = _api.Initial();

            List<Employee> employees = new List<Employee>();
            HttpResponseMessage res = await client.GetAsync("api/Employees");
            if (res.IsSuccessStatusCode)
            {
                var result = res.Content.ReadAsStringAsync().Result;
                employees = JsonConvert.DeserializeObject<List<Employee>>(result);

            }
            return View(employees);
        }

        public ActionResult Create()
        {
            try
            {
                Employee employeeModel = new Employee();
                return View(employeeModel);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create(Employee companyModel)
        {
            try
            {
                HttpClient client = _api.Initial();
                Employee employees = new Employee();
                if (ModelState.IsValid)
                {
                    var todotodoemployeeJson = new StringContent(System.Text.Json.JsonSerializer.Serialize(companyModel, null), Encoding.UTF8, "application/json");

                    HttpResponseMessage res = await client.PostAsync("/api/Employees", todotodoemployeeJson);

                    if (res.IsSuccessStatusCode)
                    {
                        var result = res.Content.ReadAsStringAsync().Result;
                        employees = JsonConvert.DeserializeObject<Employee>(result);

                    };
                }
                return View(employees);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        public async Task<IActionResult> Edit(int id)
        {
            try
            {

                HttpClient client = _api.Initial();

                Employee employees = new Employee();
                HttpResponseMessage res = await client.GetAsync("api/Employees/" + id.ToString());
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    employees = JsonConvert.DeserializeObject<Employee>(result);

                }

                return View(employees);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Employee companyModel)
        {
            try
            {
                HttpClient client = _api.Initial();
                Employee employees = new Employee();
                if (ModelState.IsValid)
                {
                    var todoemployeeJson = new StringContent(System.Text.Json.JsonSerializer.Serialize(companyModel, null), Encoding.UTF8,
                    "application/json");

                    HttpResponseMessage res = await client.PutAsync("/api/Employees", todoemployeeJson);

                    if (res.IsSuccessStatusCode)
                    {
                        var result = res.Content.ReadAsStringAsync().Result;
                        employees = JsonConvert.DeserializeObject<Employee>(result);

                    };
                }
                return View(employees);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                HttpClient client = _api.Initial();
                Employee employees = new Employee();
                HttpResponseMessage res = await client.GetAsync("api/Employees/" + id.ToString());
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    employees = JsonConvert.DeserializeObject<Employee>(result);
                }

                return View(employees);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Employee employeeModel)
        {
            try
            {
                HttpClient client = _api.Initial();
                Employee employees = new Employee();
                if (ModelState.IsValid)
                {

                    HttpResponseMessage res = await client.DeleteAsync("/api/Employees/" + employeeModel.CompanyId.ToString());

                    if (res.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Index");
                    };
                }
                return View(employees);
            }
            catch (Exception)
            {
                return View("Error");
            }
        }
    }
}
